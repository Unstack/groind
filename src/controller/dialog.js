import React from 'react'

class DialogController {
  constructor(){
    this.open = this.open.bind(this)
    this.close = this.close.bind(this)
  }
  connect(store){

    store.on("dialog.open",this.open)
    store.on("dialog.close",this.close)

    this.store = store
  }

  promiseDialogSet(dialogset){
      const d = this.promiseDialog(dialogset[0])
      var e = d
      dialogset.slice(1).map((dialog)=>
      {
        e = e.then(()=>this.promiseDialog(dialog))
      })
      return e
  }

  promiseDialog(dialog){
    return new Promise((resolve,reject)=>{
      this.open(dialog )
      this.store.once("dialog.close",resolve)
    })
  }

  questDialog(id,title,text,image){
    return {title,text,image,options:[
      {text:"Accept",color:"success",action:"quest.accept", data:id},
      {text:"Decline",color:"danger",action:"dialog.close", data:id}
    ]}
  }

  open(dialogInfo){
    this.store.get().set("dialog",dialogInfo)
  }

  close(dialogInfo){
      this.store.get().set("dialog",null)
  }
}

export default new DialogController()
