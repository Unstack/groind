class QuestController {
  constructor(){
    this.startQuest = this.startQuest.bind(this)
    this.progressQuest = this.progressQuest.bind(this)
  }

  connect(store){
    store.on("quest.start",this.startQuest)
    store.on("quest.progress",this.progressQuest)

    this.store = store
  }

  startQuest(quest){
    this.store.get().quest.push(quest)
  }

  progressQuest(questid){
    const quest = this.store.get().quest[questid]
    quest.set("progress",quest.progress+1)
    if (quest.progress > quest.length)
      this.finishQuest(questid,quest)
  }

  finishQuest(questid,quest){
    this.store.get().quest.splice(questid,1)
    this.store.emit("quest.finished",questid,quest)
  }
}

export default new QuestController()
