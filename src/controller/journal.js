class JournalController {
  constructor(){
    this.write = this.write.bind(this)
    this.close = this.close.bind(this)
  }
  connect(store){

    store.on("journal.write",this.write)
    store.on("journal.close",this.close)
    this.store = store

    store.emit("journal.write","all","Welcome back to Groin.d")

  }

  close(category){
    this.store.get().journal.remove(category)
  }
  write(category,...message){
    const {journal} = this.store.get(),
          time = new Date(),
          stamp = "["+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds()+"]"
    if (journal[category])
      journal.set(category,
      journal[category].slice(-10).concat(stamp+" "+message.join(" ")) )

    else
    {
      journal.set(category,[stamp+" "+message.join(" ")])
      this.store.emit("ui.journal.select",category)
    }

    const el = document.getElementById("journal")

    if (el)
      {
        const lastChild = el.children[el.children.length-1]
        lastChild.scrollIntoView(true)
      }
  }
}

export default new JournalController()
