class SkillController {
  constructor(){
    this.skillFired = this.skillFired.bind(this)
  }

  connect(store){
    store.on("skill.fired",this.skillFired)
    this.store = store
  }

  skillFired(skill){
    skill.set("fired",true)

    window.setTimeout(()=>
    this.store.get().character.characterClass.skillset
    .find((e) => e.name===skill.name).set("fired",false),skill.cooldown[skill.level]*1000)
  }
}

export default new SkillController()
