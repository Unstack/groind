import worldGenerator from '../generator/world'

import area        from "./area"
import combat      from "./combat"
import character   from "./character"
import progress    from "./progress"
import effect      from "./effect"
import journal     from "./journal"
import dialog      from "./dialog"
import skill       from "./skill"
import story       from "./story"
import quest       from "./quest"

const modules = [ area,combat,character,
                  progress,effect,journal,
                  dialog, skill,story,quest]

class MainController {
  connect(store){
    const state = store.get()

    modules.map((module)=>module.connect(store))

    if (!state.world)
    {
      console.log("Creating world")
      const world = worldGenerator(100)
      world[0].name = world[0].name.slice(0,world[0].name.indexOf(" "))+" Forest"
      world[1].name = "Quiet Town"
      store.get().set("world",world)
      store.emit("newGame")
    }


    store.on("ui.autohunt.set",(value)=>store.get().set("autohunt",value))
    store.on("shortcut.set",(key)=>console.log("sc",key))
    this.store = store
  }
}

export default new MainController()
