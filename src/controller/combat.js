class CombatController {
  constructor(){
    this.huntTarget     = this.huntTarget.bind(this)
    this.huntTargetAuto = this.huntTargetAuto.bind(this)
    this.huntResume     = this.huntResume.bind(this)

    this.canFight = true
  }

  connect(store){
    const {battle} = store.get()

    store.on('huntTarget',this.huntTarget)
    store.on('action:hunt.auto',this.huntTargetAuto)
    store.on("hunt.resume",this.huntResume)

    store.on("skill.activate",(skillid,battleid)=>{
        this.activateSkill("character",
        skillid,
        battleid)
    })

    store.on("opponent.death",({name})=>(
    this.store.emit("journal.write","battle","You have slain the "+name))
    )

    this.store = store

    Object.keys(battle).filter((e)=>battle[e])
          .map((e)=> this.huntResume(e,battle[e]) )
  }
  startBattle(id,battle){
    this.store.emit("battle.start", id,battle)
    this.combatCycle(id,"character")
    this.combatCycle(id,"opponent")
  }
  calculateStatistic(entity){
    return{
    damage:(entity.attribute.strength/4),
    attackSpeed:1.5-(entity.attribute.dexterity*0.01),
    maxHealth:(entity.attribute.constitution*2),
    armor:( ((entity.attribute.constitution*2) + (entity.attribute.strength/2))/4),
    healthRegen:(entity.attribute.constitution/10),
    magicDamage:((entity.attribute.intelligence/4)+(entity.attribute.wisdom/8)/2),
    magicResist:( (entity.attribute.wisdom+(entity.attribute.constitution/2)/4) ),
    shopDiscount:(entity.attribute.charisma/10),
    criticalChance:(entity.attribute.dexterity/500),
  }
  }
  spawn(target,resuming){
    const statistic = this.calculateStatistic(target)
    return Object.assign({},target,{statistic},
            resuming ? {} : {health:statistic.maxHealth})
  }

  combatCycle(id,invoker){
    window.setTimeout(()=>{
      const attacker    = invoker === "character" ? this.store.get().character
                                                 : this.store.get().battle[id],
            defender    = invoker === "character" ? this.store.get().battle[id]
                                                 : this.store.get().character,
            isCharacter = invoker === "character"
      if (!attacker || !defender)
        return
      if (attacker.health <= 0 || defender.health <= 0)
      {
        return this.battleEnd(id,invoker)
      }
      else{
        const damage = this.attack(id,attacker,defender,isCharacter)

        if (defender.health-damage <= 0)
          return this.battleEnd(id,invoker)
        else
          this.combatCycle(id,invoker)
      }
    },(invoker === "character" ? this.store.get().character
                               :this.store.get().battle[id])
        .statistic.attackSpeed*1000)
  }

  activateSkill(invoker,skillid,id) {
    const state      = this.store.get(),
          skill      = invoker === "character" ? state.character
                                                      .characterClass
                                                      .skillset[skillid]
                                               : state.battle[id]
                                                      .enemyClass
                                                      .skillset[skillid],
          attacker    = invoker === "character" ? state.character
                                                : state.battle[id],
          defender    = invoker === "character" ? state.battle[id]
                                                : state.character,
          isCharacter = invoker === "character"

    skill.effect[skill.level].length
    ? skill.effect[skill.level].map( ({action,body})=>
        this[action].apply(this,[id,attacker,defender,isCharacter].concat( body ))
      )
    : this[skill.effect[skill.level].action]
      .apply(this,[id,attacker,defender,isCharacter].concat( skill.effect[skill.level].body ))

    this.store.emit("skill.fired",skill)
  }

  attack(id,attacker,defender,isCharacter){
    var damage = attacker.statistic.damage
                              * (1-(defender.statistic.armor/
                                Math.abs(500-defender.attribute.constitution)
                              )),
         critical = Math.random()<attacker.statistic.criticalChance
    if (critical)
      damage*=2

    return this.hurt(id,attacker,defender,isCharacter,damage,critical)
  }

  hurt(id,attacker,defender,isCharacter,amount,critical){
    var target = !isCharacter ? this.store.get().character
                             : this.store.get().battle[id]

    if (!target)
        return

    target.set({health:Math.floor(target.health-amount)})

    if (isCharacter)
      this.store.emit("opponent.hurt",      id,
                                           amount,
                                           critical,
                                           target.health,
                                           target.statistic.maxHealth)
    else
      this.store.emit("character.hurt",amount,critical,target.health,target.statistic.maxHealth)

    return amount
  }

  battleEnd(id,invoker,premature=false){

    if (invoker==="opponent")
      return
    const state = this.store.get(),
          battle = state.battle[id].toJS()

    if (Object.keys(state.battle).length-1 === 0)
    {
      this.store.emit("battle.endcombo",0)
    }

    if (state.character.health <= 0)
    {
      this.store.get().character.set("dead",true)
      this.store.emit("battle.lost",id,battle)
      this.store.emit("character.death",id,battle)
    }
    else
    {
      this.store.emit("battle.won",id,battle)
      this.store.emit("character.grantexp",battle.exp)
      this.store.emit("character.grantgold",battle.gold)

      var reward = []
      for (var i=0;i<Math.floor(battle.level/10)+2;i++)
        reward.push ( battle.droptable.find((item) => Math.random()>0.5 ) )

      reward = reward.filter((e)=>e)
      if (reward.length){
        this.store.emit("character.grantitem",reward)

        for (var itemid in reward)
          this.store.emit("journal.write","battle","You found a"+
            (["a","e","i","o","u","A","E","I","O","U"]
              .indexOf(reward[itemid].name[0])===-1? "n " : " ")+reward[itemid].name
              +" ("+battle.name+")")
      }
    }

    if (Object.keys(state.battle).length-1 < state.unlocked.maxBattles)
      this.canFight = true

    window.setTimeout(()=>{
      this.store.get().battle.remove(id.toString())
      this.store.emit("battle.finished:"+id)
    },250)
  }

  huntTargetAuto(){
    const state = this.store.get().set("autohunting",true),
          delay = state.autohunt,
          world = state.world,
          autohunt = window.setInterval(()=>{
            if (!this.canFight)
              return
            const {unlocked:{maxBattles},battle,currentArea}
                    = this.store.get(),
            encounter = world[currentArea].encounter

            const randomEncounter =
                  encounter[Math.floor(Math.random()*encounter.length)]
            if (Object.keys(battle).length+1 >= maxBattles)
              this.canFight = false
            this.huntTarget(randomEncounter)
          },delay*1000)

   this.store.once("action:hunt.auto.stop",()=>{
     window.clearInterval(autohunt)
     this.store.get().set("autohunting",false)
   })

  }

  huntResume(id,opponent){
    this.huntTarget(opponent,id)
  }

  log(newBattle,battleState){
    newBattle.on("character.hurt",(amount,critical)=>
      this.store.emit("journal.write",
      "battle",
      (critical?"CRITICAL!":"")+
      "The "+battleState.opponent.name+
      " hits you for "+amount.toFixed(2)+" damage"))

    newBattle.on("opponent.hurt",(id,amount,critical)=>
    this.store.emit("journal.write","battle",
      (critical?"CRITICAL!":"")+
      "You attack the "+battleState.opponent.name+
      " for "+amount.toFixed(2)+" damage"))
  }

  huntTarget(target,_id=null){
    const state = this.store.get(),
          battle = state.battle,
          character = state.character,
          id = _id != null ? _id : Object.keys(battle).length

    if (id >= state.unlocked.maxBattles)
      return

    battle.set(id,this.spawn(target,_id == false))
    this.startBattle(id,target)

  }
}
export default new CombatController()
