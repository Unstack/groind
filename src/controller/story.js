import dialog from './dialog'

import dialogData from '../data/dialog'

class StoryModule {
  constructor(){

  }

  connect(store){
    this.store = store
    store.on("newGame",()=>
    {
      store.emit("quest.start",{title:"Mysterious awakening",
                              length:4,
                              progress:0,
                              description:`You woke up in a forest to be
                                           greeted by a strange girl. What
                                           happened, and why are you naked?`})
      dialog.promiseDialogSet(dialogData.forest_meadow)

      store.once("battle.finished:0",()=>
      {
         const repeat = (from=0)=>{
            const x = dialog.promiseDialogSet(dialogData.forest_meadow_2.slice(from))
            x.then((res)=>res==="retry"
                   ? repeat(2)
                   :dialog.promiseDialog(dialogData.confirmedClass))
            return x
                 }

        repeat()
      })

      store.once("changeArea",()=>
        dialog.promiseDialogSet(dialogData.sister_act)
        )
    })
  }
}

export default new StoryModule()
