class ProgressController {
  constructor(){
    this.statistic = {
      enemiesKilled:0,
      killsPerZone:{},
      totalExp:0,
      highestStreak:0
    }
    this.unlocked = {
      maxBattles:3,
      maxZone:0
    }

  }

  adder(category,amount=1,type="statistic")
  {
    return function(){
      this[type][category] += amount
      this.store.get().set(type,this.statistic)
    }.bind(this)
  }

  connect(store){
    this.statistic = Object.assign({},this.statistic,store.get().statistic.toJS())
    this.unlocked  = Object.assign({},this.unlocked,store.get().unlocked.toJS())


    store.on("battle.won",this.adder("enemiesKilled") )

    store.on("battle.endcombo",(combo)=>{
      if (combo > this.statistic.highestStreak)
      {
         this.statistic.highestStreak = combo
         store.get().set("statistic",this.statistic)
      }
    })
    store.on("battle.won",()=>{
      var state = store.get()
      if (this.statistic.killsPerZone[state.currentArea])
       this.statistic.killsPerZone[state.currentArea]++
      else
        this.statistic.killsPerZone[state.currentArea] = 1
      state = state.set("statistic",this.statistic)

      if (this.statistic.killsPerZone[state.currentArea] >= (state.currentArea+1)*3
         && this.unlocked.maxZone <= state.currentArea)
         {
           this.unlocked.maxZone++
           state.set("unlocked",this.unlocked)
           store.emit("area.unlocked",this.unlocked.maxZone)
           if (state.autoAdvance)
            store.emit("changeArea",this.unlocked.maxZone)
         }
    })

    store.on("character.gainedexp", (amount) => {
      this.statistic.totalExp += amount
      store.get().set("statistic",this.statistic)
    })

    store.get().set("statistic",this.statistic)
               .set("unlocked",this.unlocked)
    this.store = store
  }
}

export default new ProgressController()
