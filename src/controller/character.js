import character from '../data/character'
import generic from "../data/generic"

class CharacterController {
  constructor(){
    this.grantExp  = this.grantExp.bind(this)
    this.grantGold = this.grantGold.bind(this)
    this.grantItem = this.grantItem.bind(this)

    this.death    = this.death.bind(this)
    this.revive   = this.revive.bind(this)
    this.recovery = this.recovery.bind(this)

    this.calculateStatistic = this.calculateStatistic.bind(this)

    this.sellItem       = this.sellItem.bind(this)
    this.sellAllItems   = this.sellAllItems.bind(this)
    this.sellAllItemsOf = this.sellAllItemsOf.bind(this)

    this.equipItem  = this.equipItem.bind(this)
    this.removeItem = this.removeItem.bind(this)

    this.consume = this.consume.bind(this)

    this.attributeIncrease = this.attributeIncrease.bind(this)

    this.characterRestore = this.characterRestore.bind(this)

    this.characterAffect = this.characterAffect.bind(this)

    this.recoveryInterval = null
  }
  connect(store){
    store.on("character.grantexp",this.grantExp)
    store.on("character.grantgold",this.grantGold)
    store.on("character.grantitem",this.grantItem)

    store.on("character.death",this.death)
    store.on("character.statistic.calculate",this.calculateStatistic)

    store.on("action:sell",this.sellItem)
    store.on("action:sell.all",this.sellAllItems)
    store.on("action:sell.allOf",this.sellAllItemsOf)


    store.on("character.restore",this.characterRestore)

    store.on("action:equip",this.equipItem)
    store.on("action:remove",this.removeItem)

    store.on("action:consume",this.consume)

    store.on("action:skill.activate",this.activateSkill)

    store.on("character.affect",this.characterAffect)

    store.on("action:character.attribute.increase", this.attributeIncrease)
    store.on("character.hurt",()=>{
      if (this.recoveryInterval === null)
        this.recoveryInterval = window.setInterval(this.recovery,1000)
    })

    const character = store.get().character

    if (character.health < character.statistic.maxHealth || character.dead)
      this.recoveryInterval = window.setInterval(this.recovery,1000)

    character.characterClass.skillset.map((skill,key)=>{
        if (skill.fired)
          window.setTimeout(()=>
            store.get().character
                       .characterClass
                       .skillset[key]
                        .set("fired",false) ,skill.cooldown[skill.level]*1000)
      }
    )
    if (character.dead)
      store.once("character.fullhealth",this.revive)

    this.store = store

    store.emit("character.statistic.calculate")
  }

  calculateStatistic(){
    const character = this.store.get().character,
          attribute = character.attribute,
          oldStatistic = character.statistic,
          statistic = {
          damage:(attribute.strength/4),
          attackSpeed:1.5-(attribute.dexterity*0.01),
          accuracy:((attribute.strength*0.025)+(attribute.dexterity*0.05) ),
          maxHealth:50+(attribute.constitution*2),
          armor:( ((attribute.constitution*2)+(attribute.strength/2)) /4),
          healthRegen:(attribute.constitution/10),
          magicDamage:((attribute.intelligence/4)+(attribute.wisdom/8)/2),
          magicResist:( (attribute.wisdom+(attribute.intelligence/2)/4) ),
          shopDiscount:(attribute.charisma/10),
          criticalChance:(attribute.dexterity/500),
        }

     generic.masteries.map((mastery)=>{
        statistic["mastery_"+mastery] = 0
      })

    Object.keys(character.equipment).map((slot)=>{
      const item = character.equipment[slot]
      if (item)
        for (var name in item.statistic)
        {
          if (name === "attackSpeed")
            statistic[name] = (item.statistic.attackSpeed+statistic[name])/2
          else if (typeof item.statistic[name] === "number")
            statistic[name] += item.statistic[name]
          else if (typeof item.statistic[name] === "string")
            if (item.statistic[name].endsWith("%"))
              statistic[name] *= 1+(parseInt(item.statistic[name].slice(0,-1))/100)
        }
      return null
    })
    character.set("statistic",statistic)
             .set('health',character.health * (statistic.maxHealth/oldStatistic.maxHealth) )
  }

  characterAffect(effect,permanent=false){
    this.store.get().character.effect.push(effect)

    if (!permanent)
      window.setTimeout(()=>{
        const effects = this.store.get().character.effect

        effects.splice(effects.indexOf(effect),1)
      },effect.duration*1000)
  }
  characterRestore(type,amount){
    const character =
      this.store.get().character
    switch (type){
      case "health":
        character.set("health",character.health+amount>character.statistic.maxHealth
                               ? character.statistic.maxHealth
                               : character.health+amount)
      break
      case "mana":
      break
      default:
        return null
    }
  }
  attributeIncrease(attribute,amount=1){
      const attributes = this.store.get().character.attribute

      if (attributes.available >= amount)
        attributes.set(attribute,attributes[attribute] + amount)
                  .set("available", attributes.available - amount)
      else
      attributes.set(attribute,attributes[attribute] + attributes.available)
                .set("available", 0)

      this.store.emit("character.statistic.calculate")
  }

  consume(itemid){

    const character = this.store.get().character,
          item = character.inventory[itemid]
    item.effect.map((effectInfo)=>this.store.emit.apply(this,effectInfo))
    character.inventory.splice(itemid,1)
  }
  removeItem(slot){
    const character = this.store.get().character

    this.store.emit("character.grantitem",character.equipment[slot])
    character.equipment.set(slot,null)
  }
  equipItem(itemid){
      const character = this.store.get().character,
            inventory = character.inventory,
            item = inventory[itemid]

    inventory.splice(itemid,1)

    if (character.equipment[item.slot])
      this.store.emit("character.grantitem",character.equipment[item.slot])

    character.equipment.set(item.slot,item)

    this.store.emit("character.statistic.calculate")
  }

  sellItem(itemid){
    const state = this.store.get()
    state.character.set("gold",state.character.gold +
                               (state.character.inventory[itemid].value||0) )
    state.character.inventory.splice(itemid,1)
  }

  sellAllItems(itemid){
    const state = this.store.get(),
          value = state.character.inventory.map((item)=>item.value||0)
                                           .reduce((col,entry)=> col+entry,0)
          state.character.set("gold",state.character.gold+value)
                         .set("inventory",[])
  }

  sellAllItemsOf(itemid){
    const state = this.store.get(),
          item = state.character.inventory[itemid],
          selected = state.character.inventory.filter((e) => e.name === item.name),
          value = selected.map((item)=>item.value||0)
                                           .reduce((col,entry)=> col+entry,0)

          state.character.set("gold",state.character.gold+value)
                         .set("inventory",state.character.inventory
                                                      .filter((e) => e.name != item.name))
  }

  revive(){
    this.store.get().character.set("dead",false)
    this.store.emit("character.respawned")

        this.store.emit("journal.write","all","You feel less dead, hooray.")
  }
  death(){
    this.store.emit("action:hunt.auto.stop")
    const state = this.store.get()

    state.set("battle",{})

    state.character.set("exp",state.character.exp*0.9)
    this.store.get().set("deathMessage",generic.characterDeath[
      Math.floor(Math.random()*generic.characterDeath.length)
    ])

    this.store.once("character.fullhealth",this.revive)

    this.store.emit("journal.write","all","You have perished..")
  }

  recovery(){
    var character = this.store.get().character

    if (!character.dead)
      character = character.set("health",character.health+character.statistic.healthRegen)
    else
      character = character.set("health",character.health+
                            (character.statistic.maxHealth/((character.level+2)*2) ) )

    if (character.health >= character.statistic.maxHealth)
    {
      character.set("health",character.statistic.maxHealth)

      window.clearInterval(this.recoveryInterval)
      this.recoveryInterval = null
      this.store.emit("character.fullhealth")
    }
  }

  grantItem(item){
    if (item.length && item.length > 0)
    {
      const char = this.store.get().character
      char.set("inventory",char.inventory.concat(item))
    }
    else
      this.store.get().character.inventory.push(item)
  }
  grantGold(amount){
    const character = this.store.get().character
          character.set("gold",character.gold+amount)
  }

  grantExp(amount){
    var character = this.store.get().character
    character = character.set("exp",character.exp+amount)

    if (character.exp > character.tnl[character.level-1])
    {
        character.set("level",character.level+1)
                 .attribute.set("available",character.attribute.available+5)
        this.store.emit("character.levelup",character.level+1)
    }
    else
    {
          this.store.emit("character.gainedexp", amount)
    }
  }
}

export default new CharacterController()
