class AreaController {
  connect(store){
    this.changeArea = this.changeArea.bind(this)
    this.hunt = this.hunt.bind(this)

    store.on('changeArea',this.changeArea)
    store.on('action:hunt',this.hunt)

    this.store = store
  }
  hunt(areaID){
    const {encounter} = this.store.get().world[areaID],
          randomEncounter = encounter[Math.floor(Math.random()*encounter.length)]

    this.store.emit("huntTarget",randomEncounter)
  }


  changeArea(value){
    this.store.get().set("currentArea",value)
  }
}

export default new AreaController()
