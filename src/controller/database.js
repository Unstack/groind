import data from "../data"

class DatabaseController {


  get(path){
    return path.split(".").reduce((dataset,path)=>dataset[path],data)
  }
}

export default new DatabaseController()
