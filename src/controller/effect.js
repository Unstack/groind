import $ from 'jquery'

class EffectController {
  constructor(){
    this.characterHurt = this.characterHurt.bind(this)
    this.opponentHurt = this.opponentHurt.bind(this)
    this.characterDeath = this.characterDeath.bind(this)
  }
  connect(store){
    store.on("character.hurt",this.characterHurt)
    store.on("opponent.hurt",this.opponentHurt)
    store.on("character.death",this.characterDeath)
    this.store = store
  }

  characterDeath(){
    /*
    const character = this.store.get().character,
          time = character.statistic.maxHealth/
                (character.statistic.maxHealth/((character.level+2)*2))
                *1000*/
  }
  hitSplat(amount,critical,
            {left,top}={left:0,top:0},
            {offsetLeft,offsetTop}={offsetTop:0,offsetLeft:0}){
    const splat = document.createElement("div"),
          text = document.createElement("span")
          text.innerHTML = "-"+(amount.toFixed(2))
          text.style.fontSize = critical?"26px":"16px"

          splat.className = critical
                                ? "bg-warning text-dark pl-2 pr-2 pt-3 pb-3 shadow-lg"
                                : "bg-danger text-light p-2 pt-3 pb-3 shadow-lg"
          splat.style.border = "2px solid black"
          splat.style.transform = "rotate("+(Math.random()*40-20).toFixed(2)+"deg)"
          splat.style.borderRadius = "50%"

          splat.appendChild(text)

          splat.style.position = "fixed"
          splat.style.left =(left+offsetLeft)+"px"
          splat.style.top = (top+offsetTop)+"px"
    return splat
  }
  opponentHurt(id,amount,critical,health,max){
    const position = $("#healthbar-"+id).offset(),
          splat = this.hitSplat(amount,critical,position,
                                {offsetLeft:75,offsetTop:-50})
    $(splat).appendTo("#healthbar-"+id).css("opacity",0)
      .animate({opacity:1,top:"-="+Math.floor(Math.random()*25+25)},
              critical?450:150).delay(critical?500:250).fadeOut(critical?300:150,()=>$(splat).remove())

  }
  characterHurt(amount,critical,health,max){
    const el = $("#healthbar"),
          offset = el.offset(),
          width = el.width(),
          splat = this.hitSplat(amount,critical,offset,
                                {offsetLeft:Math.floor((width)*(health/max))-50 })

    $(splat).appendTo("#healthbar").css("opacity",0)
      .animate({opacity:1,top:"-="+Math.floor(Math.random()*25+25)},
              critical?1000:150).delay(critical?500:250).fadeOut(critical?300:150,()=>$(splat).remove())

  }
}

export default new EffectController()
