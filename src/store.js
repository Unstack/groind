import Freezer from 'freezer-js'

import store from 'store2'

import character from './data/character'

const storedData = store("groind"),

      data = Object.assign({},
        {
          character,
          currentArea:0,
          battle:{},
          autohunt:3,
          statistic:{},
          quest:[],
          unlocked:{},
          world:null,
          deathMessage:"Not big suprise",
          journal:{},
          autoAdvance:true,
          keyboard:{},
        },storedData,{autohunting:false}),

  state = new Freezer( data )

window.store = store
window.state = state

window.onbeforeunload = () => window.nuked ? null : store("groind",state.get())

window.nuke = () => {
  window.nuked = true
  store(null)
  window.location.reload()
}

window.nuked = true

export default state
