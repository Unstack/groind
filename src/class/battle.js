import Freezer from "freezer-js"
class Battle extends Freezer {

  constructor(character,opponent,id,resuming=false){
    super({character,id })
    this.get().set("opponent",this.spawn(opponent,resuming))
  }
  start(){
    this.characterCycle = this.combatCycle("character")
    this.opponentCycle = this.combatCycle("opponent")
  }
  calculateStatistic(entity){
    return{
    damage:(entity.attribute.strength/4),
    attackSpeed:1.5-(entity.attribute.dexterity*0.01),
    maxHealth:(entity.attribute.constitution*2),
    armor:( ((entity.attribute.constitution*2) + (entity.attribute.strength/2))/4),
    healthRegen:(entity.attribute.constitution/10),
    magicDamage:((entity.attribute.intelligence/4)+(entity.attribute.wisdom/8)/2),
    magicResist:( (entity.attribute.wisdom+(entity.attribute.constitution/2)/4) ),
    shopDiscount:(entity.attribute.charisma/10),
    criticalChance:(entity.attribute.dexterity/500),
  }
  }
  spawn(target,resuming){
    const statistic = this.calculateStatistic(target)
    return Object.assign({},target,{statistic},
            resuming ? {} : {health:statistic.maxHealth})
  }

  combatCycle(target){
    if (this.finished)
      return

    this[target+"Cycle"] = window.setTimeout(()=>{
      const state       = this.get(),
            attacker    = target === "character" ? state.character
                                                 : state.opponent,
            defender    = target === "character" ? state.opponent
                                                 : state.character,
            isCharacter = target === "character"

      if (this.finished || attacker.health <= 0 || defender.health <= 0 )
        return this.end()

      const damage = this.attack(attacker,defender,isCharacter)

      if (defender.health-damage <= 0)
        return this.end()

      this.combatCycle(target)

    },this.get()[target].statistic.attackSpeed*1000)
  }

  activateSkill(target,skill) {
    const battle      = this.get(),
          id          = battle.id,
          attacker    = target === "character" ? battle.character
                                               : battle.opponent,
          defender    = target === "character" ? battle.opponent
                                               : battle.character,
          isCharacter = target === "character"

    skill.effect[skill.level].length
    ? skill.effect[skill.level].map( ({action,body})=>
        this[action].apply(this,[attacker,defender,true].concat( body ))
      )
    : this[skill.effect[skill.level].action]
      .apply(this,[attacker,defender,true].concat( skill.effect[skill.level].body ))
  }

  attack(attacker,defender,isCharacter){

    var damage = attacker.statistic.damage
                              * (1-(defender.statistic.armor/
                                Math.abs(500-defender.attribute.constitution)
                              )),
         critical = Math.random()<attacker.statistic.criticalChance
    if (critical)
      damage*=2

    return this.hurt(defender,damage,isCharacter,critical)
  }

  hurt(_target,amount,isCharacter,critical){
    const target = _target.set({health:Math.floor(_target.health-amount)})

    if (isCharacter)
      this.emit("opponent.hurt",this.get().id,
                                           amount,
                                           critical,
                                           target.health,
                                           target.statistic.maxHealth)
    else
      this.emit("character.hurt",amount,critical,target.health,target.statistic.maxHealth)

    if (target.health<=0)
    this.end()
    return amount
  }

  end(premature=false){
    window.clearTimeout(this.characterCycle)
    window.clearTimeout(this.opponentCycle)

    if (this.finished)
      return

    this.finished = true

    if (!premature)
      this.emit("finished",this.get())
  }

}

Battle.index = 0

export default Battle
