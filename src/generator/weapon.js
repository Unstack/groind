import generic from '../data/generic'

const shuffle = function (b) {
  var k, t, len,
      a = b.slice(0,b.length)
  len = a.length;

  if (len < 2)
    return a;

  while (len) {
    k = Math.floor(Math.random() * len--);
    t = a[k];

    while (k < len) {
      a[k] = a[++k];
    }

    a[k] = t;
  }

  return a;
};


const weaponStatisticTable = {
  "sword": { attackSpeed:[0.5,0.8],
             damage:0.85,
             accuracy:[0.65,0.75],
             criticalChance:[0.05,0.15] },
  "dagger":{ attackSpeed:[0.3,0.6],
             damage:0.4,
             accuracy:[0.85,0.95],
             criticalChance:[0.1,0.25] },
  "axe":   { attackSpeed:[0.8,1.0],
             damage:1.5,
             accuracy:[0.5,0.75],
             criticalChance:[0.025,0.08]  },
  "spear": { attackSpeed: [0.5,0.75],
             damage:0.6,
             accuracy: [0.7,0.85],
             criticalChance:[0.075,0.125] },
  "scythe":{ attackSpeed: [0.9,1.2],
             damage:1.3,
             accuracy: [0.65,0.75],
             criticalChance:[0.05,0.15] },
  "wand": { attackSpeed: [0.9,1.1],
            damage:1,
            accuracy: [0.75,0.9],
            magicAttack:0.8,
            criticalChance:[0.05,0.11]
            },
  "staff":{ attackSpeed: [0.7,0.9],
            damage:1.1,
            accuracy: [0.65,0.85],
            magicAttack:0.85,
            criticalChance:[0.04,0.1]},
  "bow": {attackSpeed: [0.45,0.65],
          damage:0.5,
          accuracy: [0.65,0.85],
          criticalChance:[0.07,0.15] },
  "crossbow":{attackSpeed: [0.75,1],
              damage:0.75,
              accuracy: [0.75,0.95],
              criticalChance:[0.05,0.125] }
}
const floor = Math.floor,
      rFloat = (min,max) => Math.random()*max+min,
      rInt = (min,max) => floor(Math.random()*max+min),
      pickRandom = (array) => array[rInt(0,array.length)],

      weaponGenerator = (power,type=pickRandom(shuffle(Object.keys(weaponStatisticTable)))) => {
          const propset = {
            name: pickRandom(generic.adjective)+" "+type,
            level:rInt(power,Math.floor(power*2)),
            value:rInt((power+5)/2,power),
            slot:"weapon",
            icon:"/weapon/"+type+".svg",
            statistic:{}
          }

          for (var prop in weaponStatisticTable[type])
              if (weaponStatisticTable[type][prop].length)
              propset.statistic[prop] = rFloat(weaponStatisticTable[type][prop][0],
                                            weaponStatisticTable[type][prop][1])
              else
              propset.statistic[prop] = weaponStatisticTable[type][prop]*power

          return propset
}

export default weaponGenerator
