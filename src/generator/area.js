import generic from "../data/generic"

import enemyGenerator from './enemy'

const floor = Math.floor,
      areaType = [
        "hills",
        "forest",
        "lake",
        "mountain",
        "jungle",
        "plains",
        "field"
      ],
      rInt = (min,max) => floor(Math.random()*max+min),
      rColor = () => "rgb("+rInt(30,180)+","+rInt(30,180)+","+rInt(30,180)+")",
      pickRandom = (array) => array[rInt(0,array.length)],
      upperFirst = (string) => string[0].toUpperCase()+string.slice(1),
      areaGenerator = (power) => {
          const propset = {
            name:upperFirst(pickRandom(generic.adjective)+" "+
                            pickRandom(generic.noun))+" "+
                            pickRandom(areaType),
            level:power,
            biome:pickRandom(generic.biome),
            background:"linear-gradient("+
                        rInt(0,180)+"deg, "+
                        rColor()+" 0%, "+
                        rColor()+" 100%)"
          }

          propset.encounter = new Array(rInt(3,power/5))
                                       .fill(0)
                                       .map((n,i)=>enemyGenerator(propset.level) )
          return propset
}

export default areaGenerator
