import weapon     from './weapon'
import armor      from './armor'
import consumable from './consumable'

const floor = Math.floor,
      rFloat     = (min,max) => Math.random()*max+min,
      rInt       = (min,max) => floor(Math.random()*max+min),

      dropChance = {
        weapon:0.1,
        armor:0.2,
        consumable:1
      },
      dropClass = {
        weapon:weapon,
        armor:armor,
        consumable:consumable
      },
      droptableGenerator = (power) => {
        const droptable = Object.keys(dropChance).map((entry)=>{
          for (var i = 0;i<Math.floor(power/5)+1;i++)
            if (rFloat(0,1) < dropChance[entry])
              return dropClass[entry](power)

        })
        return droptable.filter((e)=>e)
}

export default droptableGenerator
