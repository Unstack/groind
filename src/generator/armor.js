import generic from "../data/generic"

const slotNames = {
  "head":"helmet",
  "body":"body armor",
  "legs":"pants",
  "feet":"boots"
},armorStatisticTable = {
 legs:{ armor: 4 },
 body: {armor: 6 },
 feet: {armor: 2.5},
 head: {armor: 3.5 }
}
const floor = Math.floor,
      rFloat = (min,max) => Math.random()*max+min,
      rInt = (min,max) => floor(Math.random()*max+min),
      pickRandom = (array) => array[rInt(0,array.length)],

      armorGenerator = (power,slot=pickRandom(Object.keys(slotNames))) => {
          const propset = {
            name: pickRandom(generic.adjective)+" "+slotNames[slot],
            level:rInt(power,Math.floor(power*2)),
            value:rInt((power+5)/2,power),
            slot,
            icon:"/armor/"+slotNames[slot].replace(" ","-")+".svg",
            statistic:{}
          }

          for (var prop in armorStatisticTable[slot])
              if (armorStatisticTable[slot][prop].length)
              propset.statistic[prop] = rFloat(armorStatisticTable[slot][prop][0],
                                            armorStatisticTable[slot][prop][1])
              else
              propset.statistic[prop] = armorStatisticTable[slot][prop]*power

          return propset
}

export default armorGenerator
