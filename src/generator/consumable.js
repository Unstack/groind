import generic from "../data/generic"

const consumableTable = {
 minor_health:{ name:"Minor health potion",
                value: 2.50,
                icon: "/consumable/potion_health.svg",
                effect: [
                    ["character.restore","health",10]
                ]},
 basic_health:{ health: 25,
                name:"Regular health potion",
                value: 5,
                icon: "/consumable/potion_health.svg"},
 major_health:{ health: 50 ,
                name:"Major health potion",
                value: 75,
                icon: "/consumable/potion_health.svg"},
 minor_mana: { mana: 8,
               name:"Minor mana potion",
               value: 4.5,
               icon: "/consumable/potion_mana.svg"},
 basic_mana: { mana: 24,
               name:"Regular mana potion",
               value: 32,
               icon: "/consumable/potion_mana.svg"},
 major_mana: { mana: 55,
              name:"Major mana potion",
              value: 94,
              icon: "/consumable/potion_mana.svg"},
}

const floor = Math.floor,
      rFloat = (min,max) => Math.random()*max+min,
      rInt = (min,max) => floor(Math.random()*max+min),
      pickRandom = (array) => array[rInt(0,array.length)],

      consumableGenerator = (power) => {
          if (power < 25)
            return consumableTable.minor_health//pickRandom([consumableTable.minor_health,consumableTable.minor_mana])
          else if (power < 50)
            return consumableTable.basic_health//pickRandom([consumableTable.basic_health,consumableTable.basic_mana])
          else
            return consumableTable.major_health//pickRandom([consumableTable.major_health,consumableTable.major_mana])

}

export default consumableGenerator
