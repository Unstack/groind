
import areaGenerator from './area'

export default (size,difficulty=1) => {
    return new Array(size).fill(0)
                          .map((k,i)=>areaGenerator((i+1)*difficulty))
}
