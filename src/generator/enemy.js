import droptable from './droptable'

import enemy from "../data/enemy"

import generic from "../data/generic"

const shuffle = function (b) {
  var k, t, len,
      a = b.slice(0,b.length)
  len = a.length;

  if (len < 2)
    return a;

  while (len) {
    k = Math.floor(Math.random() * len--);
    t = a[k];

    while (k < len) {
      a[k] = a[++k];
    }

    a[k] = t;
  }

  return a;
};

const floor = Math.floor,
      rInt = (min,max) => floor(Math.random()*max+min),
      pickRandom = (array) => array[rInt(0,array.length)],

      enemyGenerator = (power) => {
          const propset = {
            name: pickRandom(generic.adjective)+" "+pickRandom(generic.name.animal),
            level:power,
            biome:pickRandom(generic.biome),
            health:rInt(3+power*0.9,power*1.5),
            gold:rInt((power+5)/2,power),
            exp:rInt(5+power*3,power*4),
            description:null,
            attribute:{
              strength:4,
              dexterity:4,
              constitution:4,
              charisma:4,
              intelligence:4,
              wisdom:4
            },
            inventory:[]
          }

          var attributePoints = 36+(power*5),
                attribute = shuffle(["strength","dexterity","constitution",
                             "intelligence","wisdom","charisma"])
          attribute.map((name,index)=>{
            var value = 0
              if (index >= attribute.length)
                value = attributePoints
              else
                value = rInt(attributePoints/6,attributePoints/3)

            propset.attribute[name] += value
            attributePoints -= value
            return null
          })

          propset.attribute.strength += attributePoints

          propset.droptable = droptable(power)

          return Object.assign({},enemy[0],propset)
}

export default enemyGenerator
