import Freezer from 'freezer-js'

window.f = Freezer
class Freeza extends Freezer {
  constructor(name,events,calls,initialState={},context=null){
    super(initialState)
    this.name = name
    this.events = events
    this.calls = calls
    this.context = Object.assign({store:this,calls},context)
    try {
    for (var _name in events)
      this.on(_name,calls[events[_name]].bind(this.context) )
    }
    catch(e){
      console.log("ERROR: Missing callback in '"+this.name+"' for '"+_name)
    }
  }
  childUpdater({name}){
    const store = this.store
    return (state) => store.set(name,state)
  }
  connect(child){
    for (var event in child.events)
    {
      this.on(event,child.calls[ child.events[event] ].bind(child.context) )
    }
    this.get().set(child.name,child.get())
    child.on("update", this.childUpdater(child) )
  }
}

export default Freeza
