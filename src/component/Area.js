import React from 'react'

export default ({areaID,area:{name,level,encounter},
                max,
                onAreaChange,
                className}) => (
    <div className="row text-center mb-4">
      <div className="col" >
        <button className="btn btn-warning m-2"
                onClick={()=>onAreaChange(areaID-1)}
                disabled={ areaID===0  }> Previous </button>
      </div>

      <div className="col-6">
      <div className="float-right" data-tip="Area Level">
        <span className="h1">✩</span>
        <span className="h1">{level}</span>
      </div>
      <div className="float-left" data-tip="Amount of enemies in this area">
        <span className="h1">⍾</span>
        <span className="h1">{encounter.length}</span>
      </div>
        <h1 className={"flashy "+
          (name.length < 12    ? "h1"
          : name.length < 20 ? "h2"
          : name.length < 28 ? "h3"
          : name.length < 36 ? "h4"
          : name.length < 44 ? "h5"
          : "h6")
        }>
          {name}
        </h1>

      </div>
      <div className="col">
        <button className="btn btn-warning m-2"
                onClick={()=>onAreaChange(areaID+1)}
                disabled={areaID>=max}> Next </button>
      </div>
    </div>
)
