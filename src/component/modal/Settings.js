import React from 'react'

const Settings = ({store}) => (
  <div className="container">
    <div className="form-group">
      <label>Reset everything. </label>
      <button className="btn btn-danger float-right"
        onClick={window.nuke}>
          Reset Everything
        </button>
      </div>
  </div>
)

const SettingsModal = ({store}) => (
  <div className="modal fade-in"
       tabIndex="-1"
       role="dialog"
       id="settings">
  <div className="modal-dialog"
        role="document">
    <div className="modal-content">
      <div className="modal-header pb-0">
        <div className="col text-center m-0 p-0">
          <h1 className="flashy display-4 m-0 p-0">Settings</h1>
        </div>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body pt-2">
        <Settings/>
      </div>
    </div>
  </div>
</div>
)
export default SettingsModal
export {Settings}
