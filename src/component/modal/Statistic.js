import React from 'react'

const Statistic = ({character,statistic,keyboard,store}) => (
  <div className="container-fluid m-0 h-100">
    <h4 className="h4 mb-0">Attributes</h4>
    { Object.keys(character.attribute).map((attribute,key)=>(
      <div className="d-flex justify-content-between" key={key}>
        <span>{attribute}</span>
        <span>{character.attribute[attribute]}

        {character.attribute.available && attribute!="available" ?
        <button
          onClick={()=>store.emit("action:character.attribute.increase",
                       attribute,
                       keyboard.shift ? 5 :
                       keyboard.ctrl ? 10 :
                       keyboard.alt ? 25 : 1)}

          className={"btn btn-sm ml-2 mb-2 "+
          ( keyboard.shift?"btn-info"
           :keyboard.ctrl?"btn-warning"
           :keyboard.alt?"btn-danger"
           :" btn-success")}>
          + {keyboard.shift ? 5 : keyboard.ctrl ? 10 : keyboard.alt ? 25 : null}
        </button> : null}
        </span>

      </div>
    )) }
    <h4 className="h4 mt-2 mb-0">Statistics</h4>
    { Object.keys(character.statistic).filter((e)=>!e.startsWith("mastery"))
                           .map((attribute,key)=>(
      <div className="d-flex justify-content-between" key={key}>
        <span>{attribute}</span>
        <span>{character.statistic[attribute].toFixed(2)}</span>
      </div>
    )) }
    <h4 className="h4 mt-2 mb-0">Other Statistics</h4>
    {Object.keys(statistic).map((category,key)=>(
      <div className="d-flex justify-content-between" key={key}>
        <span>{category}</span>
        { typeof statistic[category] == "number" ||
          typeof statistic[category] == "string"
          ? <span>{statistic[category]}</span>
          : (<ul>
              {Object.keys(statistic[category]).map((key)=>
                <li key={key}>Zone #{parseInt(key)+1} : {statistic[category][key]} kills</li>
              )}
            </ul>)
        }
      </div>
    ))}
    <h4 className="h4 mt-2 mb-0">Achievements</h4>
    <p>TBA</p>
  </div>
)
const StatisticModal = ({character,statistic,store}) => (
  <div className="modal fade-in"
       tabIndex="-1"
       role="dialog"
       id="statistic">
  <div className="modal-dialog"
        role="document">
    <div className="modal-content">
      <div className="modal-header pb-0">
        <div className="col text-center m-0 p-0">
          <h1 className="flashy display-4 m-0 p-0">Statistics</h1>
        </div>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
        <Statistic character={character} statistic={statistic} store={store}/>
      </div>
    </div>
  </div>
</div>
)
export default StatisticModal
export {Statistic}
