import React,{Fragment} from 'react'

const Equipment = ({equipment,store}) => (
  <div className="container-fluid"
       style={{height: "inherit",overflowY:"scroll"}}>
    {Object.keys(equipment).map((slot,key)=>(
      <div className="row border-bottom justify-content-around" key={key}>
        <div className="col">{slot}</div>
          { equipment[slot]
          ? <Fragment>
              <div className="col">
                   {equipment[slot].name}
              </div>
              <div className="col">
                <ul style={{listStyle:"none"}}>
                  {Object.keys(equipment[slot].statistic)
                         .map((name)=>(
                           <li key={name}>{name}: {equipment[slot].statistic[name]}</li>
                         ))}
                </ul>
              </div>
              <div className="col-12">
                <button className="btn btn-danger"
                        onClick={()=>store.emit("action:remove",slot)}
                >R</button>
              </div>
            </Fragment>
          : <div className="col-4">
               empty
          </div>}

      </div>
    ))}

  </div>
)
const EquipmentModal = ( {character:{equipment},store }) => (
  <div className="modal fade-in"
       tabIndex="-1"
       role="dialog"
       id="equipment">
  <div className="modal-dialog"
        role="document">
    <div className="modal-content">
      <div className="modal-header pb-0">
        <div className="col text-center m-0 p-0">
          <h1 className="flashy display-4 m-0 p-0">Equipment</h1>
        </div>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div className="modal-body container pt-2">
        <Equipment equipment={equipment} store={store} />
      </div>

    </div>
  </div>
</div>
)
export default EquipmentModal
export {Equipment}
