import React,{Component,Fragment} from 'react'
import ReactDOMServer from 'react-dom/server'

import ReactTooltip from 'react-tooltip'

const Item = ({item}) => (
  <div>
    <p>{item.name}</p>
    <ul className="p-0 m-0" style={{listStyle:"none"}}>
      {Object.keys(item.statistic||{}).map((name)=>(
        <li key={name}>{name} : {item.statistic[name]}</li>
      ))}
    </ul>
    <div className="float-right">
        <span className="icon">¥</span>
        <span>{item.value||0}</span>
    </div>
  </div>
)

const Inventory = ({inventory,selected,onSelect,store}) => (
  <div className="container-fluid">
  {selected != null
    ? (<Fragment>
        <div className="row mt-2">
        <div className="col">
          <img src={"/icon/"+(inventory[selected].icon||"unknown.svg")}
               alt="item preview"/>
        </div>
        <div className="col-8">
            <h2 className="h6">{inventory[selected].name}</h2>
            <ul className="p-0" style={{listStyle:"none"}}>
              {Object.keys(inventory[selected].statistic||{}).map((name)=>(
                <li key={name} className="d-flex justify-content-between">
                  <span>{name}</span>
                  <span>{inventory[selected].statistic[name].toFixed(2)}</span>
                  </li>
              ))}
              <li>¥{inventory[selected].value||0}</li>
            </ul>
          </div>
        </div>
        <div className="row mt-2 mb-2">
          <div className="col ml-4">
            <div className="btn-group">
              {inventory[selected].slot
                ? <button
                onClick={()=>store.emit("action:equip",selected)}
                className="btn btn-sm btn-primary">Equip</button>
                : null
              }
              {inventory[selected].effect
                ? <button
                onClick={()=>store.emit("action:consume",selected)}
                className="btn btn-sm btn-primary">Consume</button>
                : null
              }
              <button
                onClick={()=>store.emit("action:sell",selected)}
              className="btn btn-sm btn-success">Sell</button>
              <button
                onClick={()=>store.emit("action:sell.allOf",selected)}
              className="btn btn-sm btn-warning">Sell all of these</button>
              <button
                onClick={()=>store.emit("action:sell.all",selected)}
              className="btn btn-sm btn-danger">Sell All</button>
            </div>
          </div>
      </div></Fragment>)
    : (<div className="row pt-4 pb-4">
        <div className="col text-center">Select an item</div>
      </div>) }

   <div className="row d-flex">
         { inventory.map((item,key)=>(
      <div key={key} className={"col-2 h-25 p-1 text-center "+(
                                selected===key ? "border border-danger":null)}
                      onClick={()=>onSelect(key)}
                      data-tip={item.name}
                      data-for="inventory">
        <img src={"/icon"+(item.icon||"unknown.svg")}
              alt={item.name}
              data-tip={ReactDOMServer.renderToStaticMarkup(<Item item={item}/>)}
              data-for="inventory" />
      </div>
      )) }
    </div>
  </div>
)

class InventoryModal extends Component {
  constructor(props){
    super(props)
    this.state = {
      selected:null
    }

    this.onSelect = this.onSelect.bind(this)
  }

  onSelect(selected){
      this.setState({selected})
  }

  render(){
    const {character:{inventory}} = this.props

    var  {selected}                   = this.state

    if (selected>=inventory.length)
      selected = null

    return (<div className="modal fade-in"
         tabIndex="-1"
         role="dialog"
         id="inventory">
         <ReactTooltip
         html={true}
         id="inventory"
         className="bg-light text-dark border border-primary shadow-lg"
         />
    <div className="modal-dialog"
          role="document">
      <div className="modal-content">
        <div className="modal-header pb-0">
          <div className="col text-center m-0 p-0">
            <h1 className="flashy display-4 m-0 p-0">Inventory</h1>
          </div>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body container pt-2">
          <Inventory inventory={inventory}
          onSelect={this.onSelect}
          selected={selected}
          store={this.props.store}/>
        </div>
      </div>
    </div>
  </div>)
  }
}

export default InventoryModal
export {Inventory}
