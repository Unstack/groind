import React from 'react'
import ReactDOMServer from 'react-dom/server'

const BaseModal = ( {title,children}) => (
  <div className="modal fade-in d-block"
       tabIndex="-1"
       role="dialog"
       id="equipment">
  <div className="modal-dialog"
        role="document">
    <div className="modal-content">
      <div className="modal-header pb-0">
        <div className="col text-center m-0 p-0">
          <h1 className="flashy display-4 m-0 p-0">{title}</h1>
        </div>
      </div>

      <div className="modal-body pt-2">
        {children}
      </div>

    </div>
  </div>
</div>
)

export default BaseModal
