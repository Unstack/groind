import React from 'react'

const Map = ({character,world,currentArea}) => (
  <div className="container-fluid">
    { world.map((area,key) => (
          <div className="row"
               style={{background:area.background}} key={key}>
            <div className="col-8">
              <h5 className="h6">{area.name}</h5>
            </div>
            <span className="col">
              <span className="icon">✩</span>
              <span>{area.level}</span>
            </span>
          </div>
        )) }
  </div>
)
const MapModal = ({character,world,currentArea}) => (
  <div className="modal fade-in"
       tabIndex="-1"
       role="dialog"
       id="map">
  <div className="modal-dialog"
        role="document">
    <div className="modal-content">
      <div className="modal-header pb-0">
        <div className="col text-center m-0 p-0">
          <h1 className="flashy display-4 m-0 p-0">Map</h1>
        </div>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body pt-2">
          <Map character={character} world={world} currentArea={currentArea}/>
      </div>
    </div>
  </div>
</div>
)
export default MapModal
export {Map}
