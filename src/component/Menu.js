import React from 'react'

const Menu = ({store,currentArea,canHunt,canAutoHunt,autohunt,autohunting,show=true}) => (
  <div className="col-4 justify-content-around mt-4 ">

        <div className="btn-group">
          <button className="btn btn-sm btn-primary "
                  onClick={()=> store.emit('action:hunt',currentArea) }
                  disabled={canHunt}>
            Hunt Single
          </button>

          <button className={"btn btn-sm "+(autohunting ? "btn-warning":"btn-info")}
                  disabled={canAutoHunt}
                  onClick={()=> autohunting ? store.emit('action:hunt.auto.stop')
                                            : store.emit('action:hunt.auto',currentArea) }
                  >
            <span>Hunt every </span>
            <input type="number"
                  className="control text-center p-0 m-0"
                  style={{width:"25%"}}
                  min={0}
                  step={0.25}
                  onClick={(e)=>e.stopPropagation()}
                  disabled={autohunting || canAutoHunt}
                  onChange={({target:{value}}) =>
                                store.emit("ui.autohunt.set",value)}
                  value={autohunt} />
            <span> seconds</span>
          </button>
          </div>
  </div>
)

export default Menu
