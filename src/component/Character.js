import React from 'react'

export default ({character:{name,health,exp,level,tnl,gold,dead,
                            attribute:{str,dex,int},
                            statistic:{damage,attackSpeed,maxHealth}
                  },
                  store,
                  currentArea,
                  canHunt,autohunt,autohunting
                } ) => (
      <div className="col">
        <p className="m-0">{name}</p>

            <div className="progress" id="healthbar" >
              <div className={"progress-bar "+ (dead?"bg-secondary":"bg-danger")}
                   role="progressbar"
                   aria-valuenow={health}
                   aria-valuemin="0"
                   aria-valuemax={maxHealth}
                   style={{width:Math.floor(health / maxHealth*100)+"%"}}>
                   <span>
                      {
                        dead
                        ? <span className="h1">{"☠".repeat(level)}</span>
                        : (health / maxHealth*100).toFixed(2)+"%"
                      }
                   </span>
              </div>
          </div>
       <p className="m-0 float-right">
          <span>¥ </span>
          <span>{gold.toFixed(2)}</span>
       </p>
       <p className="m-0">Level: {level} ({((exp-(tnl[level-2]||0))/tnl[level-1]*100).toFixed(2)}%)</p>

     </div>)
