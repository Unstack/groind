import React from 'react'

const Questlog = ({quest:quests,store}) =>
(<div className="container">
  {
    quests.length===0?<p>You're not currently on a quest</p>:null
  }
  {quests.map((quest)=>(
    <div className="row border border-dark">
      <div className="col-4">
        <img src="https://via.placeholder.com/128x128"/>
      </div>
      <div className="col-8">
        <h1 className="h1">{quest.title}</h1>
        <p>
          {quest.description}
        </p>
      </div>
    </div>
  ))}
</div>)

export default Questlog
