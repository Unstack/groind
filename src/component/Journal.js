import React from 'react'

const Journal = ({journal,journalTab,store}) => (
  <div className="container p-0 m-0">
  <ul style={{listStyle:"none",height:"15vh",overflowY:"scroll"}} id="journal">
    {(journal[journalTab]||[]).map((message,key)=> <li key={key}>{message}</li>)}
  </ul>
    <div className="d-flex">
      {Object.keys(journal).map((category)=>(
        <button className="btn btn-secondary p-2 pl-4 pr-4" key={category}
                onClick={()=>store.emit("ui.journal.select",category)}>{category}

                {/*<a onClick={()=>store.emit("journal.close",category)}
                  className="btn btn-sm btn-danger m-0 p-0 ml-2"
                  style={{borderRadius:"90%",width:"25px",height:"25px"}}
                  href="/#">X</a>*/}
                </button>
      ))}
    </div>

  </div>
)

export default Journal
