import React from 'react'


export default ({character,battle:battleSet,className,store} ) => (
  <div className={className||"row p-4"} style={{height:"50vh"}}>
    { Object.keys(battleSet).length === 0 ? (
      <div className="col-12">
        <p className="text-center">Not currently fighting anything</p>
      </div>
    ) : Object.keys(battleSet).map((key,id,idk,opponent=battleSet[key]) =>
      opponent === null
      ? (<div className="col col-4" key={key}></div>)
      : (
      <div className="col col-4"
            key={key}>
        <div className="bg-light rounded shadow-lg p-2">
          <span className="float-right">
            <span className="icon">✩</span>
            <span>{opponent.level}</span>
          </span>

          <h1 className={"d-none d-xl-block "+
            (opponent.name.length < 8    ? "h1"
            : opponent.name.length < 16 ? "h2"
            : opponent.name.length < 24 ? "h3"
            : opponent.name.length < 32 ? "h4"
            : opponent.name.length < 40 ? "h5"
            : "h6")
          }>
            {opponent.name}
          </h1>
          <span className="d-block d-xl-none font-weight-bold">
            {opponent.name}
          </span>

          <img className="w-100 d-none d-lg-block"
               src="https://via.placeholder.com/128x64"
               alt="Card cap"/>

          <p className="d-none d-xl-block
                        font-italic text-center mb-0">
            {opponent.description}
          </p>


          <div className="progress" id={"healthbar-"+key}>
            <div className="progress-bar bg-danger"
                 role="progressbar"
                 aria-valuenow={opponent.health}
                 aria-valuemin="0"
                 aria-valuemax={opponent.statistic.maxHealth}
                 style={{width:Math.floor(opponent.health/
                                    opponent.statistic.maxHealth*100)+"%"}}
                 >
                 <span>
                    {(opponent.health/
                      opponent.statistic.maxHealth*100).toFixed()+"%"}
                 </span>
            </div>
          </div>
          <div className="d-flex">
          {character.characterClass.skillset
                 .filter((skill) => skill.type === "combat" )
                 .map((skill,skillKey)=>(
                    <img  src={"/icon"+skill.icon}
                          key={skillKey}
                          className={"img-fluid skill "+(skill.fired?"fired":null)}
                          style={{width:"32px"}}
                          onClick={()=>
                            skill.fired ? null
                            : store.emit("skill.activate",skillKey,key)}/>
                ))}
          </div>

          <div className="d-none d-xl-flex
                          justify-content-around mt-2 mb-0">
            <div>
                  <span className="icon">♡</span>
                  <span>{opponent.statistic.maxHealth.toFixed(2)}</span>
            </div>
            <div>
                  <span className="icon">☉</span>
                  <span>{opponent.statistic.armor.toFixed(2)}</span>
            </div>
          </div>

         <div className="d-md-none d-xl-flex
                        justify-content-around mb-0">
           <div>
                 <span className="icon">⟳</span>
                 <span>{opponent.statistic.attackSpeed.toFixed(2)}s</span>
           </div>
           <div>
                 <span className="icon">⚔</span>
                 <span>{opponent.statistic.damage.toFixed(2)}</span>
           </div>
         </div>

        </div>
       <div>
      </div>

      </div>
    )
    )}

  </div>
)
