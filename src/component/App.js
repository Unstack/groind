import React,{Component,Fragment} from 'react'
import { HotKeys } from 'react-keyboard'
import ReactTooltip from 'react-tooltip'

import ErrorHandler from './ErrorHandler'

import Area      from './Area'
import Character from './Character'
import Combat    from './Combat'
import Menu      from './Menu'
import Journal   from './Journal'
import Questlog  from './Questlog'

import BaseModal   from './modal/BaseModal'

import{Inventory}  from './modal/Inventory'
import {Map}       from './modal/Map'
import {Equipment} from './modal/Equipment'
import {Settings}  from './modal/Settings'
import {Statistic} from './modal/Statistic'


import store    from '../store'

window.state = store

const prettyTabName = {
    "inventory":"Inventory",
    "equipment":"Equipment",
    "map":"World Map",
    "statistic":"Statistics",
    "settings":"Settings",
  }

const tabs = [
  {text:"Area",target:"area"},
  {text:"Questlog",target:"quest"},
  {text:"World Map",target:"map"},
  {text:"Cyril's Shop",target:"shop"},


]

const DeathMessage = ({deathMessage}) => (
  <div className="row justify-content-center text-light">
     <div className="col">
       <h1 className="mt-4 display-2 text-center flashy fade-in-slow">
          You are dead
      </h1>
       <p className="text-center">
         <iframe width="560" height="315" src="https://www.youtube.com/embed/XTgFtxHhCQ0?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
       </p>
     </div>
    </div>
)
class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      activeTab:"statistic",
      selected:null,
      journalTab:"all",
      mainTab:"area"
    }

    this.keyMap = {
      shift_down:['shift'],
      shift_up:{
        combo:['shift'],
        eventType:'keyup'
      },
      ctrl_down:['ctrl'],
      ctrl_up:{
        combo:['ctrl'],
        eventType:'keyup'
      },
      alt_down:['alt'],
      alt_up:{
        combo:['alt'],
        eventType:'keyup'
      }
    }

    this.handlers = {
      shift_up:(e)   => store.get().keyboard.set("shift",false),
      shift_down:(e) => store.get().keyboard.set("shift",true),
      ctrl_up:(e)    => store.get().keyboard.set("ctrl",false),
      ctrl_down:(e)  => store.get().keyboard.set("ctrl",true),
      alt_up:(e)     => store.get().keyboard.set("alt",false),
      alt_down:(e)   => store.get().keyboard.set("alt",true)

    }

    this.onSelect = this.onSelect.bind(this)
  }

  onSelect(selected){
      this.setState({selected})
  }

  componentDidMount(){
    store.on("ui.tab.select",(activeTab)=>
                              this.setState({activeTab}))
    store.on("ui.journal.select",(journalTab)=>
                              {
                                this.setState({journalTab})

                              })
    store.on("setTab",(mainTab)=>{
      this.setState({mainTab})
    })
    store.on('update',() => this.forceUpdate() )
  }

  render() {
    const state = store.get(),
          {character,currentArea,battle,world,deathMessage,
          autohunt,autohunting,statistic,unlocked,journal,dialog,
          keyboard,quest} = state,
          {activeTab,journalTab,mainTab} = this.state

    var selected = this.state.selected
    if (selected >= character.inventory.length)
      selected = null

    if (world === null)
      return (<div className="text-center"><p>Loading</p></div>)
    return (
      <ErrorHandler store={store}>
      <HotKeys keyMap={this.keyMap} handlers={this.handlers} />
        <ReactTooltip
        place="top"
        html={true}
        delayHide={0}
        delayShow={0}
        store={store}
        className="bg-light text-dark border border-primary shadow-lg"  />

        <div id="backdrop" className="position-fixed transition-bg"
            style={{  background: character.dead
                                  ? "linear-gradient(45deg, rgba(180,45,64,0.9) 0%, rgba(140,36,15,1) 100%)"
                                  : world[currentArea].background }}/>

        <div id="foreground" className="position-fixed transition-bg "
            style={{backgroundRepeat:"repeat",
                    background: character.dead
                                ? "url(texture/skulls.png)"
                                : world[currentArea].foreground}}/>

    {dialog
      ? <BaseModal title={dialog.title}>
          <div className='container'>
              <div className='row'>
              {dialog.image
                ? <div className='col-4 mb-2'>
                  <img src={dialog.image}
                        className='w-100 h-auto border border-dark rounded'/>

                </div>
                : null }
                <div className='col'>
                  <p>{dialog.text}</p>
                </div>
               </div>
               <div className='row'>
                <div className='col'>
                  {dialog.options.map((option,key)=>
                      <button key={key}
                              className={"btn btn-"+(option.color||"primary")+
                                          " "+(option.className:"")}
                              onClick={()=>{
                                Array.isArray(option.action)
                                ? option.action.map((action,key)=>store.emit(action,option.data[key]))
                                : store.emit(option.action,option.data)
                                }
                              }
                              data-tip={option.tip}
                              data-for="modal">
                        {option.text}
                      </button>
                  )}

                </div>
               </div>
            </div>
        </BaseModal>

      : null}

    <div className="container-fluid h-100">
      <div className="row h-75" >
        <div className="col-8 container-fluid p-0">
        <ul className="nav mb-2"
            style={{background:"rgba(210,210,210,0.5)"}}>
            {tabs.map((tab,key)=>
            (<li className="nav-item" key={key}>
              <a className={"nav-link text-dark font-weight-bold "+
                              (mainTab===tab.target?"active":"")}
                 onClick={()=>store.emit("setTab",tab.target)}
                 href="/#">
                {tab.text}
              </a>
            </li>))
            }
        </ul>
        {character.dead
        ? <DeathMessage deathMessage={deathMessage}/>
        : (mainTab === "area"
           ? <Fragment>
             <Area areaID={currentArea}
                   area={world[currentArea]}
                   max={unlocked.maxZone}
                   onAreaChange={(value) =>
                     store.emit('changeArea',parseInt(value,10))} />
             <Combat character={character} battle={battle} store={store}/>
             {ReactTooltip.rebuild()}
           </Fragment>
           : mainTab === "map"
           ? <div className="container-fluid p-0 m-0"
                  style={{overflowY:"scroll",height:"65vh"}}><Map world={world} store={store}/></div>
           : mainTab === "quest"
           ? <Questlog quest={quest} store={store}/>
           : null
          ) }
         </div>
         <div className="col-4 container-fluid">
          <div className="row" style={{height: "10vh",background:"rgba(210,210,210,0.5)"}}>
            <Character character={character} store={store}/>

            <div className="nav-item dropdown col p-0">
              <a className="nav-link dropdown-toggle text-dark rounded text-center"
                style={{background:"rgba(210,210,210,0.8)"}}
                id="navbarDropdown"
                role="button"
                href="/"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
                {activeTab}
              </a>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              {Object.keys(prettyTabName).map((uglyName)=>
                <button className="dropdown-item m-0" key={uglyName}
                    onClick={()=>store.emit("ui.tab.select",uglyName)}
                    disabled={activeTab===uglyName}>
                    {prettyTabName[uglyName]}
                </button>
              )}
            </div>
           </div>
        </div>
          <div className="row" style={{height:"65vh",
                                      overflowY:"scroll",
                                      background:"rgba(210,210,210,0.5)"}}>
            {{
              "inventory":<Inventory inventory={character.inventory}
                                    onSelect={this.onSelect}
                                    selected={selected}
                                    store={store}/>,
              "map":<Map world={world} store={store}/>,
              "equipment":<Equipment equipment={character.equipment} store={store}/>,
              "statistic":<Statistic statistic={statistic}
                                     character={character}
                                     store={store}
                                     keyboard={keyboard} />,
              "settings":<Settings/>
            }[activeTab]}
          </div>
        </div>
      </div>

       <div className="row h-25" style={{background:"rgba(210,210,210,0.6)"}}>
         <div className="col-8">
            <Journal journal={journal}
                     journalTab={journalTab}
                     store={store}/>
         </div>
         <Menu store={store} currentArea={currentArea}
               canHunt={Object.keys(battle)
                         .filter((e) => battle[e] )
                         .length > 2 || character.dead }
               canAutoHunt={character.dead}
               autohunt={autohunt}
               autohunting={autohunting} />
       </div>

    </div>
    </ErrorHandler>)
  }
}
export default App;
