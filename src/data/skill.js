export default [
  { name:"Double Strike",
    description: "Instantly attack your opponent twice.",
    icon:"/skill/blade-drag.svg",
    type:"combat",
    className:"warrior",
    level:0,
    maxLevel:4,
    cooldown:[5,4.5,4,3.5,3],
    fired:0,
    effect:
      [ [{action:"attack"},{action:"attack"}],
        [{action:"attack"},{action:"attack"}],
        [{action:"attack"},{action:"attack"}],
        [{action:"attack"},{action:"attack"}],
        [{action:"attack"},{action:"attack"}] ] ,

  },
  { name:"Enhanced Combat",
    description: "Increases damage done with swords, axes and spears.",
    type:"passive",
    className:"warrior",
    level:0,
    maxLevel:0,
    cooldown:[5,4,3,2,1],
    fired:0,
    effect:[
      ["character.statistic.set",{ mastery:{sword:10,spear:10,axe:10} }]
    ]
  }
]
