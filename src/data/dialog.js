export default {
  confirmedClass:{
    title:"Forest Meadow 2",
    text:`
        Without any further explanation the girl turns around and legs it,
        dissapearing into the canopy without a trace. You decide it might be
        for the better to get out of the forest on your own, since that was
        clearly leading to nothing.
        `,
    options:[
      { text:"What the hell is wrong with her",
        action:"dialog.close",className:"col mb-2 btn-primary"
      }
    ]
  },
  forest_meadow:[{
    title:"Forest Meadow",
    text:`
        You wake up in a forest meadow. Sunlight is shining through the trees
         warming the damp grass. You seem to be naked and covered in dirt.
         As you sit up from your nap you glance around and notice a girl
         standing by the edge of the meadow, staring at you. As she notices
        you wake she comes towards you.
    `,
    options:[
      { text:">",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    image:"/face/tamako.jpg",
    text:`
        ..Are you dead?
    `,
    options:[
      { text:"Wait what? No, of course not.",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    image:"/face/tamako.jpg",
    text:`
      \"Well you've been laying there for almost the entire day.
      When I found you this morning I figured you were, y'know, gone. For a
      second I thought you might have risen as a zombie or something. You
      know how those things go.\"
    `,
    options:[
      { text:"..What? Wait where am I?",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    image:"/face/tamako.jpg",
    text:`
      \"You mean you don't know? I thought zombies didn't get amnesia. I mean
      you're eating brains the entire time you'd think you could memorise a
      thing or two.\"
    `,
    options:[
      { text:"Woman what is wrong with you, I am not a zombie. Now tell me where on earth I am.",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    image:"/face/tamako.jpg",
    text:`
      \"It's just the forest next to Quiet Town, no need to go berserk. I figured
      you looked like a knowledged adventurer. Perhaps you were a rich merchant
      when you were still alive, too busy counting coin to read maps? Oh that
      sounds so exciting. Do you remember anything at all since you've died?\"
    `,
    options:[
      { text:"No not really. Not dead by the way. Again.",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    image:"/face/tamako.jpg",
    text:`
      \"Well how can you be so sure? You say you don't remember anything, sounds
      like a zombie to me.\"
    `,
    options:[
      { text:"Why did you leave me here all day in the first place? You could've called someone.",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    image:"/face/tamako.jpg",
    text:`
      \"Well I told my sister I found a zombie in the forest but she told me to
      quit making things up and get back to work. I snuck out again to see how
      long it would take for you to,'rise again and consume the earth'
      and all that stuff. But you wouldn't get up so I just went into town for
      snacks. When I came back I had snacks and watched you decay some more.
      Then you woke up and now you're a zombie.\"
    `,
    options:[
      { text:"There has to be something seriously wrong with you",
        action:"dialog.close",className:"col mb-2"},
    ]
  },
  {
    title:"Forest Meadow",
    text:`
      Before she gets to explain anything, a sudden gawking noise behind you
      draws your attention. As you turn around you see something emerge from
      the underbrush and charge towards you.
    `,
    options:[
      { text:"Protect the girl",
        action:["action:hunt","dialog.close"],
        data:[0],
        className:"col mb-2"},
    ]
  }],

  forest_meadow_2:[{
   title:"Forest Meadow 2",
   text:`\"T-that was.. amazing. You're like a hero. So you're not a zombie
           for real. So that means you came back from the dead alive. That is
           even more amazing. \"`,
   image:"/face/tamako.jpg",
   options:[
     { text:"Uh, don't worry about it",
       action:"dialog.close",className:"col mb-2 btn-primary"
     }
   ]
 },
 {
   title:"Forest Meadow 2",
   text:"\" ...could I ask you a strange question?\"",
   image:"/face/tamako.jpg",
   options:[
     { text:"Can't be any stranger than you right? Shoot",
       action:"dialog.close",className:"col mb-2 btn-primary"
     }
   ]
 },
 {
   title:"Class Select",
   text:"\"Tell me hero-kun, how would you make love to a woman?\"",
   image:"/face/tamako.jpg",
   options:[
     { text:"Like a truck (Warrior)",
       action:["character.set.class","dialog.close"],
       data:["warrior"],
       className:"col mb-2"},
     { text:"Worship her feet (Paladin)",
     action:["character.set.class","dialog.close"],
     data:["paladin"],className:"col mb-2"},
     { text:"Not into women (Barbarian)",
     action:["character.set.class","dialog.close"],
     data:["barbarian"],className:"col mb-2"},
     { text:"Lots of foreplay (Magician)",
     action:["character.set.class","dialog.close"],
     data:["magician"],className:"col mb-2"},
     { text:"Watch my dog do her (Ranger)",
     action:["character.set.class","dialog.close"],
     data:["ranged"],className:"col mb-2"},
     { text:"Not while she's breathing (Necromancer)",
     action:["character.set.class","dialog.close"],
     data:["necromancer"],className:"col mb-2"},
     { text:"Without her noticing (Rogue)",
     action:["character.set.class","dialog.close"],
     data:["rogue"],className:"col mb-2"},

   ]
 },
 {
   title:"Class Select",
   text:"\"That is absolutely pathetic, what on earth is wrong with you?\"",
   image:"/face/tamako.jpg",
   options:[
     { text:"But it's natural",
       action:"dialog.close",className:"col mb-2 btn-success"
     },
     { text:"I'll rethink my past choices",
       action:"dialog.close",className:"col mb-2 btn-danger",
       data:"retry",
     },

   ]
 }],
  sister_act:[{
     title:"Sister Act",
     text:`\"Oh hey, you must be that adventurer Tamako was going on about.
           Sorry about her storming off on you, she tends to be like that
           around men. If you ever need anything we run a general store over there.
           Well it's mostly me really. Tamako tends to just hang around in town
           all day. \"`,
     image:"/face/cyril.jpg",
     options:[
       { text:"You already sound a lot more sane than your sister.",
         action:"dialog.close",className:"col mb-2"}
     ]
   },
   {
     title:"Sister Act",
     text:`\"You'd be suprised how often I hear that. You wouldn't be the first
             guy she had a crush on. Not by a mile. Aren't you cold at all
             running around naked by the way? `,
     image:"/face/cyril.jpg",
     options:[
       { text:"Kind of, you don't suppose you could spare some clothing?",
         action:"dialog.close",className:"col mb-2"}
     ]
   },
   {
     title:"Sister Act",
     text:`\"Of course, as long as you have the coins I've got you covered. `,
     image:"/face/cyril.jpg",
     options:[
       { text:"I'm kind of broke.",
         action:"dialog.close",className:"col mb-2"}
     ]
   },
   {
     title:"Sister Act",
     text:`\"Wow okay, so you're kind of a bum then. I don't really feel like
             associating with you in that case. Have a nice day being naked sir. \" `,
     image:"/face/cyril.jpg",
     options:[
       { text:`I woke up in the mud to be socially fondled by your sister,
               even after saving her, and this is how you treat me?`,
         action:"dialog.close",className:"col mb-2"}
     ]
   },
   {
     title:"Sister Act",
     text:`\"I'm suprised you didn't go for the whole 'I saved her life' thing
             there. Well thank you mister hero for keeping my sister off her job
             for the entire day and looking at your dead broke ass. Maybe if that
             monster got her I would have some quiet time in the store at least.
             But no, you bring her back alive and in tears. Thanks so much. \" `,
     image:"/face/cyril.jpg",
     options:[
       { text:`What on earth is wrong with you?`,
         action:"dialog.close",className:"col mb-2"}
     ]
   },
   {
     title:"Sister Act",
     text:`\"Oh I don't know. Maybe my business is failing with the lack of paying
             customers in this area. Maybe it's my sister who refuses to work for
             longer than half a shift before sneaking out to goof off in town.
             Maybe it's all the deadbeat jerks who she drags in who come looking
             for a handout because 'Look at me, I'm so naked!'. Why don't YOU
             decide what's wrong with me for yourself? \" `,
     image:"/face/cyril.jpg",
     options:[
       { text:`You're stressed.`,
         action:"dialog.close",className:"col mb-2"}
     ]
   },
   {
     title:"Sister Act",
     text:`\"Because you're totally a therapist. You know what, this
             conversation is over, I'm done with you. If you don't have any coin
             go and look around for some monsters to beat up and take theirs.
             THEN come to the store and I'll give you some clothes at a
             DISCOUNT. We do this ONE time and one time only. And ONLY because
             you saved Tamako. Don't get any ideas because I'm not giving away
             any more free stuff. \" `,
     image:"/face/cyril.jpg",
     options:[
       { text:`Alright fine, I'll be down there in a little bit just take it easy`,
         action:"dialog.close",className:"col mb-2"}
     ]
   },

   ,
  ]
}
