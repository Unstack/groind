import area      from './area'
import enemy     from './enemy'
import character from './character'
import progress  from './progress'
import statistic from './statistic'

export default {
  area,
  enemy,
  character,
  progress,
  statistic
}
