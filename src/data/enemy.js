export default [
  {name:"Sand Crab",
  description:"Permanently salty",
  health:8,
  exp:4.5,
  level:1,
  biome:["sandy"],
  gold:1,
  effect:[],
  drop:[
    ["Crab Legs",0.1,1,4]
  ],
  statistic:{
    attackSpeed:0.6,
    damage:0.1,
    maxHealth:8,
    armor:0.2,
    critChance:0
  }},
  {name:"Squid",
  description:"Unstable mess of tentacles",
  health:5,
  exp:3.5,
  level:1,
  biome:["sandy"],
  gold:1,
  drop:[
    ["Tentacle",0.1,1,4]
  ],
  statistic:{
    attackSpeed:0.4,
    damage:0.15,
    maxHealth:5,
    armor:0.1
  }},
  {name:"Bucket of sand",
  description:"Eats fire for breakfast",
  health:15,
  exp:6,
  level:1,
  biome:["sandy"],
  gold:2.5,
  drop:[
    ["Crab Legs",0.1,1,4]
  ],
  statistic:{
    attackSpeed:1,
    damage:0.4,
    maxHealth:15,
    armor:0.2
  }},
]
