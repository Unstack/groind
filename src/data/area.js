import enemy from "./enemy"

export default [
  null,
  { name:"Beach Zone", level:1,
    encounter:[ enemy[0],enemy[1], enemy[2] ],
    background:"linear-gradient(15deg, rgba(64,145,139,1) 0%, rgba(187,190,75,1) 100%)",
  },
  { name:"Jungle Zone", level:2,
    encounter:[ enemy[0] ],
    background:"linear-gradient(45deg, rgba(72,145,64,0.9) 0%, rgba(145,116,39,0.7) 100%)",
  },
  { name:"Rock Zone", level:3,
    background: "gray",
    encounter:[ enemy[0] ]},
  {name:"Desert Zone", level:4,
   background: "rgba(144,144,50,1)",
   encounter:[ enemy[0] ]}
]
