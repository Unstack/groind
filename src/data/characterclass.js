import skill from './skill'

export default [
  { name:"Warrior",
    skillset:skill.filter((skill) => skill.className === "warrior"),
  }
]
