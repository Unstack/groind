import characterclass from './characterclass'

export default {
  name:"Unnamed player",
  characterClass:characterclass[0],
  health:100,
  level:1,
  mana:0,
  gold:0,
  exp: 0,
  dead:false,
  attribute:{
    strength:94,
    dexterity:4,
    constitution:4,
    intelligence:4,
    wisdom:4,
    charisma:4,
    available:36
  },
  statistic:{
    attackSpeed:0.7,
    damage:1,
    maxHealth:100,
    healthRegen:0.25,
    armor:0,
    accuracy:0,
    mastery:{ sword:0,
              spear:0,
              axe:0,
              bow:0,
              dagger:0,
              magic_fire:0,
              magic_earth:0,
              magic_water:0,
              magic_wind:0}
  },
  equipment:{
    head:{name:"Flimsy Cap",statistic:{armor:2}, slot:"head",icon:"/armor/helm.svg" },
    body:{name:"Flimsy Armor",statistic:{armor:4}, slot:"body", icon:"/armor/scale-mail.svg" },
    legs:{name:"Flimsy Legging",statistic:{armor:3}, slot:"legs",icon:"/armor/armored-pants.svg"  },
    feet:{name:"Flimsy Sandals",statistic:{armor:1}, slot:"feet",icon:"/armor/steeltoe-boots.svg"  },
    weapon:{name:"Crowbar",statistic:{damage:1}, slot:"weapon",icon:"/weapon/crowbar.svg"  },
    offHand:null
  },
  inventory:[],
  effect:[],
  tnl:[70,133,241,417,690,1097,1679,2483,3551,4926,6644,8729,11194,14039,17251,20803,24657,28769,33087,37557,42123,46732,51334,55883,60341,64673,68851,72855,76668,80279,83682,86874,89857,92634,95211,97594,99793,101816,103674,105377,106934,108357,109654,110835,111910,112887,113774,114578,115307,115967,116565,117106,117595,118036,118436,118796,119121,119415,119680,119919,120134,120329,120504,120662,120804,120932,121048,121152,121245,121330,121406,121474,121536,121591,121641,121686,121727,121763,121796,121826,121852,121876,121898,121917,121935,121951,121965,121978,121989,121999,122009,122017,122025,122031,122037,122043,122048,122052,122056,122060]
}
